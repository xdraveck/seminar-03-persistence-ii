package cz.fi.muni.pa165.tasks;

import cz.fi.muni.pa165.entity.Category;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;


public class Task01 {

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Test
	public void categoryTest() {
		Category cat;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			cat = new Category();
			cat.setName("Test");
			em.persist(cat);
			em.getTransaction().commit();
		} finally {
			if (em != null) em.close();
		}

		//TODO under this line: create a second entity manager in categoryTest and use the find() method to find
		// the category and assert its name. Note that Category uses GenerationType.IDENTITY.

	}
}
